package com.fluidnotions;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.fluidnotions.entities.Employee;
import com.fluidnotions.repositories.EmployeeRepository;

@SpringBootApplication
@ComponentScan
public class DemoApplication {

	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);
	
	@Autowired
	private EmployeeRepository employeeData;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@PostConstruct
	public void demo() {
		Employee employee1 = new Employee("John", "Jones", Employee.Gender.MALE.getGender());
		Employee employee2 = new Employee("Bill", "Boston", Employee.Gender.MALE.getGender());
		Employee employee3 = new Employee("Robin", "Rabbit", Employee.Gender.FEMAIL.getGender());
		
		List<Employee> emps = new ArrayList<>();
		emps.add(employee1);
		emps.add(employee2);
		emps.add(employee3);
		
		employeeData.save(emps);
	
		
		log.info("Show all employees: ");
		log.info("-------------------");
		for (Employee employeeDO : employeeData.findAll()) {
			log.info(employeeDO.toString());
		}


	}



}
