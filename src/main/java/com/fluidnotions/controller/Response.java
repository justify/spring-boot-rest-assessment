package com.fluidnotions.controller;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


public class Response {
	
	private ObjectMapper om = new ObjectMapper();
	
		//1=true,0=false,2=simultaneous update
		int status;
		String msg;
		Object json;

		public Response(int status, String msg) throws JsonGenerationException, JsonMappingException, IOException {
			this(status, msg, null);

		}

		public Response(int status) throws JsonGenerationException, JsonMappingException, IOException {
			this(status, null, null);
		}

		public Response(int status, String msg, Object jsonArg) throws JsonGenerationException, JsonMappingException, IOException {
			super();
			this.status = status;
			this.msg = msg;
	
			this.json = jsonArg;
		}
		
		public Response() {
			super();
		}

		public String toJson() throws JsonGenerationException, JsonMappingException, IOException{
			return om.writeValueAsString(this);
		}

		public int getStatus() {
			return status;
		}
 
		public void setStatus(int status) {
			this.status = status;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public Object getJson() {
			return json;
		}

		public void setJson(Object json) {
			this.json = json;
		}

		
		
		

	}