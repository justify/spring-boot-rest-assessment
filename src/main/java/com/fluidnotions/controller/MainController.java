package com.fluidnotions.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fluidnotions.entities.Employee;
import com.fluidnotions.repositories.EmployeeRepository;

/**
 * Created by fluidnotions
 */

@RestController
public class MainController {

	@Autowired
	private EmployeeRepository employeeData;

	@Autowired
	private ObjectMapper om;

	@RequestMapping(value = "/addOrEditEmployee", method = RequestMethod.POST)
	public Response addOrEditEmployee(@RequestBody Employee employee)
			throws JsonGenerationException, JsonMappingException, IOException {

		// List<Employee> emps = employeeData.findByAll(employee.getName(),
		// employee.getSurname(), employee.getGender());

		employeeData.save(employee);
		// design flaw issues with duplicate names when more then one result is
		// found
		if (employee != null && employee.getEmpId() == null) {
			return new Response(1, "new created", employee);

		}
		return new Response(2, employee.getName() + " " + employee.getSurname()
				+ " updated!", employee);

	}

	@RequestMapping(value = "/getEmployee", method = RequestMethod.GET)
	public Response getEmployee(@RequestParam(value = "id") String search)
			throws JsonGenerationException, JsonMappingException, IOException {

		Employee emp = employeeData.findOne(new Long(search.trim()));
		return new Response(1, emp.getName() + " " + emp.getSurname(), emp);

	}

	private boolean checkDelta(Employee emp, Employee empUp) {
		if (emp.getGender().equals(empUp.getGender())
				&& emp.getName().equals(empUp.getName())
				&& emp.getSurname().equals(empUp.getSurname())) {
			return true;
		}
		return false;

	}

	@RequestMapping(value = "/listEmployees", method = RequestMethod.GET)
	public Response employees() throws JsonGenerationException,
			JsonMappingException, IOException {
		List<Employee> allEmployees = (List<Employee>) employeeData.findAll();
		return new Response(1, allEmployees.size() + " employees exist!",
				allEmployees);

	}

}