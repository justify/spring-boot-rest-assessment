package com.fluidnotions.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.fluidnotions.entities.Employee;


@Controller
public class WebSocketController {


	@MessageMapping("/push" )
    @SendTo("/topic/refresh")
    public Message deltaVariancePush(Employee input) throws Exception {
		return new Message();
       
    }
    
    
}
