package com.fluidnotions.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
public class Employee {
	
	public enum Gender {
	   MALE(1), FEMAIL(2);
	   
	   private int value;

	   Gender(int value) { this.value = value; }

	      public int getGender() { return value; }
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long empId;
	@Column(name = "name")
	private String name;
	@Column(name = "surname")
	private String surname;
	@Column(name = "gender")
	private Integer gender;
	
	
	

	public Employee(String name, String surname, Integer gender) {
		super();
		this.name = name;
		this.surname = surname;
		this.gender = gender;

	}
	
	public Employee() {
		super();
	
	}
	
	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}


	

	
	@Override
	public String toString() {
		return "Employee [userId=" + empId + ", name=" + name + ", surname="
				+ surname + ", gender=" + gender + "]";
	}

	
	
	

}
