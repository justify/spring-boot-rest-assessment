package com.fluidnotions.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.fluidnotions.entities.Employee;

/**
 * Created by justin
 */
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	
//	@Query("SELECT p FROM Employee p WHERE LOWER(p.name) = LOWER(?1) and LOWER(p.surname) = LOWER(?2) and p.gender) = ?3")
//	List<Employee> findByAll(String name, String surname, Integer gender);

}
