var $ = global.sphra$ = require("jquery");
var utils = require('./utils');
require('./noConflictEditedDeps/bootstrap');
var JSON2 = require('JSON2');
var domSetup = utils.DomSetup();
var sockJS = require("./sockjs-0.3.4");
var stomp = require("./stomp");
var EmployeeEntityUI = function EmployeeEntityUI(options) {
    var host = options.host || "http://localhost:8080"
    var publicInterface;
    var spinnerTargetDiv = "#spinner";
    var empId = null;
    var debugging = true;
    var ajx = utils.AjaxWrap();
    var gwl = utils.QuickGrowl();
    var spin = utils.Spinner();
    var tmpls = utils.TemplateUtils({
            mountpoint: host,
            temaplatePath: "/main/templates/"
        }),
        getEndPoint = function getEndPoint(base) {
            return "/";
        },
        init = function init() {
            $(".container-fluid").css("height:600px");
            spin.start();
            return tmpls.renderExtTemplate({
                name: 'ui',
                selector: "#emps"
            }).then(function() {
                return ajx.ajaxGetJson(getEndPoint() + "listEmployees")
            }).then(function(data) {
                if (debugging) console.log("response: " + JSON.stringify(data));
                gwl.grrr({
                    message: "<b>" + data.msg + "</b>.",
                    type: 'success',
                    displaySec: 5
                });
                if (debugging) console.log("list: " + JSON.stringify(data.json));
                var data2 = {},
                    employees = data.json;
                data2.employees = employees;
                if (debugging) console.log("prep data: " + JSON.stringify(data2));
                return tmpls.renderExtTemplate({
                    name: 'employeeList',
                    selector: "#emps-list",
                    data: data2
                });
            }).then(function() {
                return tmpls.renderExtTemplate({
                    name: 'employeeAddOrEdit',
                    selector: "#addEditEmployeeForm"
                });
            }).then(function() {
                $(".list-group-item").on("click", function(e) {
                    e.preventDefault();
                    var insp = e;
                    var id = e.currentTarget.id;
                    console.log("id: " + id);
                    $(".list-group > li.active").removeClass("active");
                    $(e.currentTarget).addClass("active");
                    empId = id;
                    loadEmployee(id);
                });
                spin.stop();
            }).then(function() {
                $("#addEditEmployee").on("click", function(e) {
                    var male = $("#m").hasClass("active");
                    var empae = {
                        "empId": empId,
                        "name": $("#employeeForm input[name=name]").val(),
                        "surname": $("#employeeForm input[name=surname]").val(),
                        "gender": male ? 1 : 2
                    };
                    if (empId !== null) {
                        gwl.grrr({
                            message: "<b>" + empae.name + "&nbsp;" + empae.surname + " updated</b>.",
                            type: 'success',
                            displaySec: 5
                        });
                    } 


                   // if (debugging === true && empId === null) {
                   //      gwl.grrr({
                   //          message: "<b> empId is null: " + empId + "</b>.",
                   //          type: 'danger',
                   //          displaySec: 10
                   //      });
                   //  }
                    ajx.ajaxPostJson(getEndPoint() + "addOrEditEmployee", empae).then(function() {
                        return ajx.ajaxGetJson(getEndPoint() + "listEmployees")
                    }).bind({}).
                    then(function(data) {
                        if (debugging) console.log("response: " + JSON.stringify(data));
                        gwl.grrr({
                            message: "<b>" + data.msg + "</b>.",
                            type: 'success',
                            displaySec: 3
                        });
                        if (debugging) console.log("list: " + JSON.stringify(data.json));
                        var data2 = {},
                            employees = data.json;
                        data2.employees = employees;
                        if (debugging) console.log("prep data: " + JSON.stringify(data2));
                        return tmpls.renderExtTemplate({
                            name: 'employeeList',
                            selector: "#emps-list",
                            data: data2
                        });
                    }).then(function() {
                        $("#" + empId).addClass("active");
                    }).bind();
                });
                $("#clearEmployee").on("click", function(e) {
                    clearForm();
                });
            });
            if (empId !== null) loadEmployee(empId);
        },
        lookupEmployee = function lookupEmployee(id) {
            var req = {};
            req.id = id;
            spin.start();
            return ajx.ajaxGet(getEndPoint() + "getEmployee", req).then(function(resp) {
                spin.stop();
                return resp;
            });
        },
        loadEmployee = function loadEmployee(id) {
            return lookupEmployee(id).then(function(resp) {
                console.log("id: " + id);
                gwl.grrr({
                    message: "<b>" + resp.msg + "</b>.",
                    type: 'success',
                    displaySec: 3
                });
                var emp = resp.json;
                $("#employeeForm input[name=name]").val(emp.name);
                $("#employeeForm input[name=surname]").val(emp.surname);
                if (emp.gender === 1) {
                    $("#m").addClass("active");
                    $("#f").removeClass("active");
                } else {
                    $("#f").addClass("active");
                    $("#m").removeClass("active");
                }
            })
        },
        clearForm = function clearForm() {
            $("#employeeForm input[name=name]").val("");
            $("#employeeForm input[name=surname]").val("");
            $("#genderBtns > label.active").removeClass("active");
            empId = null;
        },
        lookupEmployee = function lookupEmployee(id) {
            var req = {};
            req.id = id;
            spin.start();
            return ajx.ajaxGet(getEndPoint() + "getEmployee", req).then(function(resp) {
                spin.stop();
                return resp;
            });
        }
    publicInterface = {
        init: init
    }
    return publicInterface;
}
global.sphra$(function() {
    var empui = null;
    var empuinthandler = setInterval(function() {
        if (!empui) {
            clearInterval(empuinthandler);
            empui = EmployeeEntityUI({});
            empui.init();
        }
    }, 800);
});