# Given Specs #

Write a simple Spring Boot application using an in-memory H2 Database. 

The application needs 3 rest endpoints :

1. Get a list of employees (id, name, surname, gender)
2. Add an employee to the database given an object (name, surname, gender)
3. Update an employee given an employee object (name, surname, gender) with the amended attributes.

start the usual way (java -jar target/spring-boot-h2-0.0.1-SNAPSHOT.jar) and browse to http://localhost:8080/index.html

# When I mentioned to the recruitment agent it was too easy she suggested adding features

1. add a frontend using js gulp/browserify - some standard js modules I wrote and use

![browserify.png](https://bitbucket.org/repo/qKLRXp/images/1142263445-browserify.png)

![load.png](https://bitbucket.org/repo/qKLRXp/images/1836830707-load.png)

![afterAdd.png](https://bitbucket.org/repo/qKLRXp/images/939982790-afterAdd.png)

~~2. Am going to make the frontend reactive using websocket to push updates from the server side – polling is no fun.~~ 

## Conceptual Outline

I can pull code from some few dozen projects and adapt it. 

~~Add a server push through tcp/ws/stomp.
(I wrote a licencing app server and then an jvm agent on the customer's server a while back that used spring 4 webstocket support to constantly watch for attempts to hack out the licence expiry disabling code)~~

   ~~However I would like to make the point, this design is redundant. A skilled developer would never put this into production the complexity is unnecessary, would be better to use the server hardware, since java is efficient enough, keep the code simple and simply poll for delta variation and refresh accordingly, unless there where heavy hardware restrictions or high load expected and docker/kubernetes based horizontal scaling wasn't an option.~~


## More examples of complete non-commercial projects

I do have a very in-depth project I never got paid for so it's not commercially copyrighted, send me a team name and I can share it with you on bitbucket it's two modules **ForeignKeyConstraintWalkerProxyServer (ui) _SpringBatchConstraintAwareXmlExport (job runner – parallel)** *It allows partial exports subsections of an RDBMS database tracing all the constraints so that the subsequent imports won't fail due to constraint violations. The the client's ERP database had 11000 tables, constraint chain branching 30 levels deep.--I couldn't find anything like it in the java oss space (usually jump start a project on an oss base), the client went with a lower budget existing c# alternative and did an iframe integration, I finished it in my spare time cause it was super cool getting it to work finally*